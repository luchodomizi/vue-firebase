import {
  createStore
} from 'vuex'
import router from '../router/index'

export default createStore({
  state: {
    tareas: [],
    tarea: {
      id: '',
      nombre: '',
      categorias: [],
      estado: '',
      descripcion: ''
    },
    user: null
  },
  mutations: {
    setUser(state, payload) {
      state.user = payload
    },
    cargar(state, payload) {
      state.tareas = payload
    },
    set(state, payload) {
      state.tareas.push(payload)
    },
    eliminar(state, payload) {
      state.tareas = state.tareas.filter(item => item.id !== payload)
    },
    tarea(state, payload) {
      //Validación de ruta equivocada
      if (!state.tareas.find(item => item.id === payload)) {
        router.push('/')
        return
      }
      state.tarea = state.tareas.find(item => item.id === payload)
    },
    update(state, payload) {
      state.tareas = state.tareas.map(item => item.id === payload.id ? payload : item)
      router.push('/')
    }
  },
  actions: {

    cerrarSesion({commit}){
      commit('setUser', null)
      router.push('/login')
      localStorage.removeItem('usuario')
    },

    async ingresoUsuario({commit}, usuario) {
      try {

        const res = await fetch('https://identitytoolkit.googleapis.com/v1/accounts:signInWithPassword?key=AIzaSyDrCju1Z19GyjKEn0qVBsi4dPv43JgDSos', {
          method: 'POST',
          body: JSON.stringify({
            email: usuario.email,
            password: usuario.password,
            returnSecureToken: true
          })
        })
        const userDB = await res.json()

        if (userDB.error) {
          console.log(userDB.error)
        }
        commit('setUser', userDB)
        router.push('/')
        localStorage.setItem('usuario', JSON.stringify(userDB))
        
      } catch (error) {
        console.log(error)
      }
    },
    async registrarUsuario({commit}, usuario) {
      try {
        const res = await fetch('https://identitytoolkit.googleapis.com/v1/accounts:signUp?key=AIzaSyDrCju1Z19GyjKEn0qVBsi4dPv43JgDSos', {
          method: 'POST',
          body: JSON.stringify({
            email: usuario.email,
            password: usuario.password,
            returnSecureToken: true
          })
        })
        const userDB = await res.json()

        if (userDB.error) {
          console.log(userDB.error.message)
          return
        }

        commit('setUser', userDB)
        router.push('/')
        localStorage.setItem('usuario', JSON.stringify(userDB))

      } catch (error) {
        console.log(error)
      }
    },
    async getDatabase({commit, state}) {
      //Verifica si hay un usuario logueado en localstorage y hace el commit para que no haga la consulta nuevamente a al db
      if(localStorage.getItem('usuario')){
        commit('setUser', JSON.parse(localStorage.getItem('usuario')))
      }else{
        return commit('setUser', null)
      }

      //Consulta a la DB
      try {
        const res = await fetch(`https://crud-vue-api-default-rtdb.firebaseio.com/tareas/${state.user.localId}.json?auth=${state.user.idToken}`);
        const dataDB = await res.json();
        const arrayTareasDB = []
        for (let id in dataDB) {
          arrayTareasDB.push(dataDB[id])
        }
        commit('cargar', arrayTareasDB)

      } catch (error) {
        console.log(error)
      }
    },
    async setTareas({commit, state}, tarea) {
      try {
        const res = await fetch(`https://crud-vue-api-default-rtdb.firebaseio.com/tareas/${state.user.localId}/${tarea.id}.json?auth=${state.user.idToken}`, {
          method: 'PUT',
          headers: {
            'Content-Type': 'application/json'
          },
          body: JSON.stringify(tarea)
        });
        const dataDB = await res.json();

        commit('set', dataDB);

      } catch (error) {
        console.log(error)
      }
    },
    async deleteTareas({commit, state}, id) {
      try {
        await fetch(`https://crud-vue-api-default-rtdb.firebaseio.com/tareas/${state.user.localId}/${id}.json?auth=${state.user.idToken}`, {
          method: 'DELETE',
          body: JSON.stringify(id)
        })
        commit('eliminar', id)

      } catch (error) {
        console.log(error)
      }
    },

    setTarea({commit}, id) {
      commit('tarea', id)
    },

    async updateTarea({commit, state}, tarea) {
      try {
        const res = await fetch(`https://crud-vue-api-default-rtdb.firebaseio.com/tareas/${state.user.localId}/${tarea.id}.json?auth=${state.user.idToken}`, {
          method: 'PATCH',
          body: JSON.stringify(tarea)
        })
        const dataDB = await res.json()
        commit('update', dataDB)
      } catch (error) {
        console.log(error)
      }
    }
  },
  getters:{
    usuarioAutenticado(state){
      return !!state.user
    }
  },
  modules: {},
})